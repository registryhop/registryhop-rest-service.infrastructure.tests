﻿using NUnit.Framework;
using System.Web;
using System.Web.Mvc;
using Moq;
using RegistryHopRestService.Infrastructure.Filters;
using System.Web.Routing;
using System;
using System.Collections.Specialized;

namespace RegistryHopRestService.Infrastructure.Tests.Filters
{
    [TestFixture]
    public class AllowCORSTest
    {
        [Test]
        public void GivenAFilterContextWhenEnablingCORSThenExpectDefaultCORSHeadersToBeSet()
        {
            var enableCORSAttribute = new EnableCORS();
            var actualHeaders = new NameValueCollection();

            var filterContext = CreateFilterContext(ref actualHeaders);

            enableCORSAttribute.OnActionExecuting(filterContext);

            Assert.IsNotNull(actualHeaders);
            Assert.IsNotEmpty(actualHeaders);

            Assert.Contains(EnableCORS.AccessControlAllowHeadersHeader, actualHeaders.AllKeys);
            Assert.Contains(EnableCORS.AccessControlAllowMethodsHeader, actualHeaders.AllKeys);
            Assert.Contains(EnableCORS.AllowControlAllowOriginHeader, actualHeaders.AllKeys);
        }

        private ActionExecutingContext CreateFilterContext(ref NameValueCollection headers)
        {
            var mockHttpResponseBase = new MockHttpResponseBase(ref headers);

            var mockHttpContext = new Mock<HttpContextBase>();
           
            mockHttpContext.SetupGet(mHC => mHC.Response)
                .Returns(mockHttpResponseBase);

            var requestContext = new RequestContext();
            requestContext.HttpContext = mockHttpContext.Object;

            var filterContext = new ActionExecutingContext();
            filterContext.RequestContext = requestContext;

            return filterContext;
        }
    }

    class MockHttpResponseBase : HttpResponseBase
    {
        public NameValueCollection MockHeaders
        {
            get;
            set;
        }

        public MockHttpResponseBase(ref NameValueCollection headers)
        {
            this.MockHeaders = headers;
        }

        public override void AddHeader(string name, string value)
        {
            this.MockHeaders.Add(name, value);
        }
    }
}
